// /// Reference to the material icon font.
pub static MATERIAL_ICONS_REGULAR_FONT: &'static [u8; 128180] =
    include_bytes!("MaterialIcons-Regular.ttf");
pub static ROBOTO_REGULAR_FONT: &'static [u8; 145348] = include_bytes!("Roboto-Regular.ttf");

// font sizes
pub static FONT_SIZE_12: f64 = 12.0;

// icon sizes
pub static ICON_FONT_SIZE_12: f64 = 12.0;
